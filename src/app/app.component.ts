import { Component } from '@angular/core';
import { Router, NavigationEnd } from '@angular/router';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  path: string = '';
  hostname = "";
  routes = "";

  constructor(private router: Router) {
    this.router.events.subscribe(event => {
      if (event instanceof NavigationEnd) {
        this.path = window.location.pathname;
        let hostname = window.location.hostname;
        let route = `https://www.sharebuilder401k.com/${event.urlAfterRedirects}`;

        this.hostname = hostname;
        this.routes = route;
      }
    });
  }
}
